########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
                  flash, g, session, redirect, url_for

# Import models
from .models import Configuration
from .models import Device


# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_configuration = Blueprint('configuration', __name__, url_prefix='/configuration')

@mod_configuration.context_processor
def store():
    store_dict = { 'serviceName': 'Configuration',
                   'serviceDashboardUrl': url_for('configuration.dashboard'),
                   'serviceBrowseUrl': url_for('configuration.browse'),
                   'serviceNewUrl': url_for('configuration.new'),
                 }
    return store_dict

# Set the route and accepted methods
@mod_configuration.route('/', methods=['GET'])
def configuration():
    return render_template('configuration/configuration_dashboard.html')


@mod_configuration.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('configuration/configuration_dashboard.html')


@mod_configuration.route('/browse', methods=['GET'])
def browse():
    return render_template('configuration/configuration_browse.html')


@mod_configuration.route('/new', methods=['GET', 'POST'])
def new():
    return render_template('configuration/configuration_new.html')


@mod_configuration.route('/profile', methods=['GET', 'POST'])
def profile():
    return render_template('configuration/configuration_profile.html')


@mod_configuration.route('/view', methods=['GET', 'POST'])
def configuration_view():
    return render_template('configuration/configuration_view.html')
